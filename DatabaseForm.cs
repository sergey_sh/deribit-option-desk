﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Deribit.Objects;
using Deribit.Database;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npgsql;

namespace deribit_option_desk
{
    public partial class DatabaseForm : Form
    {
        public DatabaseForm()
        {
            InitializeComponent();

            loadDefaultSettings();
        }

        private void saveDefaultSettingsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(
                "Do you want to override existing default parameters?",
                "Confirmation",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information
                );

            if (res == DialogResult.OK)
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@"database_settings.json"))
                {
                    Database dbSettings = Database.CurrentDatabase;

                    dbSettings.Host = hostTextBox.Text;
                    dbSettings.Port = int.Parse(portTextBox.Text);
                    dbSettings.DatabaseName = databaseNameTextBox.Text;
                    dbSettings.Username = usernameTextBox.Text;
                    dbSettings.Password = passwordTextBox.Text;

                    string jsonString = JsonConvert.SerializeObject(dbSettings, Formatting.Indented);

                    writer.Write(jsonString);
                }
            }
        }

        private void loadDefaultSettingsButton_Click(object sender, EventArgs e)
        {
            loadDefaultSettings();
        }

        private void loadDefaultSettings()
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"database_settings.json"))
            {
                string res = reader.ReadToEnd();

                var settings = JsonConvert.DeserializeObject<Database>(res);

                if (settings != null)
                {
                    hostTextBox.Text = settings.Host;
                    portTextBox.Text = settings.Port.ToString();
                    databaseNameTextBox.Text = settings.DatabaseName;
                    usernameTextBox.Text = settings.Username;
                    passwordTextBox.Text = settings.Password;
                }
            }
        }

        private void databaseConnectButton_Click(object sender, EventArgs e)
        {
            Database.CurrentDatabase.Connect();
            this.Close();
        }


        private void UpdateDatabaseSettings()
        {
            Database.CurrentDatabase.Host = hostTextBox.Text;
            Database.CurrentDatabase.Port = int.Parse(portTextBox.Text);
            Database.CurrentDatabase.DatabaseName = databaseNameTextBox.Text;
            Database.CurrentDatabase.Username = usernameTextBox.Text;
            Database.CurrentDatabase.Password = passwordTextBox.Text;
        }

        private void hostTextBox_TextChanged(object sender, EventArgs e)
        {
            Database.CurrentDatabase.Host = hostTextBox.Text;
        }

        private void portTextBox_TextChanged(object sender, EventArgs e)
        {
            Database.CurrentDatabase.Port = int.Parse(portTextBox.Text);
        }

        private void databaseNameTextBox_TextChanged(object sender, EventArgs e)
        {
            Database.CurrentDatabase.DatabaseName = databaseNameTextBox.Text;
        }

        private void usernameTextBox_TextChanged(object sender, EventArgs e)
        {
            Database.CurrentDatabase.Username = usernameTextBox.Text;
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            Database.CurrentDatabase.Password = passwordTextBox.Text;
        }
    }
}
