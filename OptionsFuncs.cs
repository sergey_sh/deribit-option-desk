﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Deribit.Objects;

namespace Deribit.OptionsFuncs
{
    /// <summary>
    /// Adapted code from http://www.risk256.com/
    /// </summary>

    public partial class BlackScholes
    {
        public static double BlackScholesImpliedVol(
            double price,
            double strike,
            double underlyingPrice,
            double yearsToExpiry,
            double riskFreeRate,
            double dividendYield,
            OptionType putCallFlag
            )
        {
            const double tolerance = 0.001;
            const int maxLoops = 16;

            double vol = Math.Sqrt(2 * Math.Abs(Math.Log(underlyingPrice / strike) / yearsToExpiry + riskFreeRate));    //Manaster and Koehler intial vol value
            vol = Math.Max(0.01, vol);
            double vega;
            double impliedPrice = BlackScholesPriceAndVega(strike, underlyingPrice, yearsToExpiry, vol, riskFreeRate, dividendYield, putCallFlag, out vega);

            int nLoops = 0;
            while (Math.Abs(impliedPrice - price) > tolerance)
            {
                nLoops++;
                if (nLoops > maxLoops)
                {
                    return 0;
                    //throw new Exception("BlackScholesImpliedVol did not converge.");
                }

                vol = vol - (impliedPrice - price) / vega;
                if (vol <= 0)
                    vol = 0.5 * (vol + (impliedPrice - price) / vega); //half way btwn previous estimate and zero
                impliedPrice = BlackScholesPriceAndVega(strike, underlyingPrice, yearsToExpiry, vol, riskFreeRate, dividendYield, putCallFlag, out vega);
            }
            return vol;
        }

        private static double BlackScholesPriceAndVega(
            double strike,
            double underlyingPrice,
            double yearsToExpiry,
            double vol,
            double riskFreeRate,
            double dividendYield,
            OptionType putCallFlag,
            out double vega
            )
        {
            double sqrtT = Math.Sqrt(yearsToExpiry);
            double d1 = (Math.Log(underlyingPrice / strike) + (riskFreeRate - dividendYield + 0.5 * vol * vol) * yearsToExpiry) / (vol * sqrtT);
            double d2 = d1 - vol * sqrtT;
            if (putCallFlag == OptionType.call)
            {
                double N1 = Distributions.StandardNormalCumulativeDistributionFunction(d1);
                double N2 = Distributions.StandardNormalCumulativeDistributionFunction(d2);
                double nn1 = Distributions.StandardNormalProbabilityDensityFunction(d1);

                vega = underlyingPrice * Math.Exp(-dividendYield * yearsToExpiry) * nn1 * sqrtT;
                return N1 * underlyingPrice * Math.Exp(-dividendYield * yearsToExpiry) - N2 * strike * Math.Exp(-riskFreeRate * yearsToExpiry);
            }
            double Nn1 = Distributions.StandardNormalCumulativeDistributionFunction(-d1);
            double Nn2 = Distributions.StandardNormalCumulativeDistributionFunction(-d2);
            double n1 = Distributions.StandardNormalProbabilityDensityFunction(d1);

            vega = underlyingPrice * Math.Exp(-dividendYield * yearsToExpiry) * n1 * sqrtT;
            return Nn2 * strike * Math.Exp(-riskFreeRate * yearsToExpiry) - Nn1 * underlyingPrice * Math.Exp(-dividendYield * yearsToExpiry);
        }

        public static void BlackScholesGreeks(
            double strike,
            double underlyingPrice,
            double yearsToExpiry,
            double vol,
            double riskFreeRate,
            double dividendYield,
            OptionType putCallFlag,
            out double delta,
            out double gamma,
            out double vega,
            out double theta,
            out double rho,
            out double convexity
            )
        {
            //setting dividendYield = 0 gives the classic Black Scholes model
            //setting dividendYield = foreign risk-free rate gives a model for European currency options, see Garman and Kohlhagen (1983)

            double sqrtT = Math.Sqrt(yearsToExpiry);
            double d1 = (Math.Log(underlyingPrice / strike) + (riskFreeRate - dividendYield + 0.5 * vol * vol) * yearsToExpiry) / (vol * sqrtT);
            double d2 = d1 - vol * sqrtT;
            double N1 = Distributions.StandardNormalCumulativeDistributionFunction(d1);
            double N2 = Distributions.StandardNormalCumulativeDistributionFunction(d2);
            double n1 = Distributions.StandardNormalProbabilityDensityFunction(d1);
            double n2 = Distributions.StandardNormalProbabilityDensityFunction(d2);

            double eNegRiskFreeRateTimesYearsToExpiry = Math.Exp(-riskFreeRate * yearsToExpiry);
            double eNegDivYieldYearsToExpiry = Math.Exp(-dividendYield * yearsToExpiry);

            double price;
            if (putCallFlag == OptionType.call)
                price = N1 * underlyingPrice * eNegDivYieldYearsToExpiry - N2 * strike * eNegRiskFreeRateTimesYearsToExpiry;
            else
                price = (1 - N2) * strike * eNegRiskFreeRateTimesYearsToExpiry - (1 - N1) * underlyingPrice * eNegDivYieldYearsToExpiry;

            if (putCallFlag == OptionType.call)
                delta = eNegDivYieldYearsToExpiry * N1;
            else
                delta = eNegDivYieldYearsToExpiry * (N1 - 1.0);

            gamma = eNegDivYieldYearsToExpiry * n1 / (underlyingPrice * vol * sqrtT);
            vega = underlyingPrice * Math.Exp(-dividendYield * yearsToExpiry) * n1 * sqrtT;

            if (putCallFlag == OptionType.call)
                rho = yearsToExpiry * strike * eNegRiskFreeRateTimesYearsToExpiry * N2;
            else
                rho = -yearsToExpiry * strike * eNegRiskFreeRateTimesYearsToExpiry * (1 - N2);

            double a = -underlyingPrice * eNegDivYieldYearsToExpiry * n1 * vol / (2.0 * sqrtT);
            double b = dividendYield * underlyingPrice * eNegDivYieldYearsToExpiry;
            if (putCallFlag == OptionType.call)
                theta = a + b * N1 - riskFreeRate * strike * eNegRiskFreeRateTimesYearsToExpiry * N2;
            else
                theta = a - b * (1 - N1) + riskFreeRate * strike * eNegRiskFreeRateTimesYearsToExpiry * (1 - N2);

            if (putCallFlag == OptionType.call)
                convexity = rho * ((n2 * sqrtT) / (N2 * vol) - yearsToExpiry);
            else
                convexity = -rho * ((n2 * sqrtT) / ((1 - N2) * vol) + yearsToExpiry);

            //return price;
        }
    }
}
