﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using deribit_option_desk;
using Newtonsoft.Json.Linq;
using Deribit.OptionsFuncs;
using ScottPlot;
using Npgsql;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using static Deribit.Database.Database;

namespace Deribit.Objects
{
    public enum OptionType
    {
        put,
        call
    }

    //singleton class for connection settings
    public class ConnectSettings
    {
        private ConnectSettings() { }

        private static readonly Lazy<ConnectSettings> lazy =
            new Lazy<ConnectSettings>(() => new ConnectSettings());

        public static ConnectSettings CurrentConnectSettings
        {
            get
            {
                return lazy.Value;
            }
            set
            {

            }
        }

        public string apiInterface { get; set; }
        public string serverAddress { get; set; }
        public string clientID { get; set; }
        public string clientSecret { get; set; }
    }

    //classes for available instrument list
    public class Instrument
    {
        public double tick_size { get; set; }
        public double taker_commission { get; set; }
        public double strike { get; set; }
        public string settlement_period { get; set; }
        public string settlement_currency { get; set; }
        public bool rfq { get; set; }
        public string quote_currency { get; set; }
        public string price_index { get; set; }
        public string? option_type { get; set; }
        public double min_trade_amount { get; set; }
        public double maker_commission { get; set; }
        public string kind { get; set; }
        public bool is_active { get; set; }
        public string instrument_name { get; set; }
        public int instrument_id { get; set; }
        public long expiration_timestamp { get; set; }
        public long creation_timestamp { get; set; }
        public string counter_currency { get; set; }
        public double contract_size { get; set; }
        public double block_trade_tick_size { get; set; }
        public int block_trade_min_trade_amount { get; set; }
        public double block_trade_commission { get; set; }
        public string base_currency { get; set; }
        public ICollection<TickData> Ticks { get; set; }
    }

    public class InstrumentsResponse
    {
        public string jsonrpc { get; set; }
        public List<Instrument> result { get; set; }
        public long usIn { get; set; }
        public long usOut { get; set; }
        public int usDiff { get; set; }
        public bool testnet { get; set; }
    }

    //classes for option book datapoints
    public class Quote
    {
        public long timestamp { get; set; }
        public string instrument_name { get; set; }
        public long change_id { get; set; }
        public List<List<double>> bids { get; set; }
        public List<List<double>> asks { get; set; }
    }

    public class Params
    {
        public string channel { get; set; }
        public Quote data { get; set; }
    }

    public class BookResponse
    {
        public string jsonrpc { get; set; }
        public string method { get; set; }
        public Params @params { get; set; }
    }

    //class for the overall market book
    public class CurrentBook
    {
        private CurrentBook()
        {
            Entries = new Dictionary<string, BookEntry>();
            AvailableExpirationDates = new List<string>();
            UnderlyingPrices = new Dictionary<string, double>();
        }

        private static readonly Lazy<CurrentBook> lazy =
            new Lazy<CurrentBook>(() => new CurrentBook());

        public static CurrentBook Data
        {
            get
            {
                return lazy.Value;
            }
            set
            {

            }
        }
        public Dictionary<string, BookEntry> Entries { get; set; } //The Dict Key ~'BTC-25MAR23-20000'
        public List<string> AvailableExpirationDates { get; set; }
        public Dictionary<string, double> UnderlyingPrices { get; set; } //holds underlying prices ('BTC' and 'ETH', perpetual futures)
    }
    public class BookEntry
    {
        public string base_currency { get; set; }
        public long timestamp { get; set; }
        public long expiration_timestamp { get; set; }
        public double strike { get; set; }
        public OptionType option_type {get; set;}
        public Bid bid { get; set; }
        public Ask ask { get; set; }
    }
    public class Bid
    {
        public double Price { get; set; }
        public double Quantity { get; set; }
    }
    public class Ask
    {
        public double Price { get; set; }
        public double Quantity { get; set; }
    }

    //class for option data for DataGridView
    //Add INotifyPropertyChanged?
    public class Options: INotifyPropertyChanged
    {
        private Options()
        {
            Dictionary<double, int> RowMap = new Dictionary<double, int>();
            HashSet<double> Strikes = new HashSet<double>();
            Dictionary<double, StrikeData> Series = new Dictionary<double, StrikeData>(); // Dict<strike, StrikeData>
        }

        private static readonly Lazy<Options> lazy =
            new Lazy<Options>(() => new Options());

        public static Options Data
        {
            get
            {
                return lazy.Value;
            }
            set
            {

            }
        }

        //Selected Base Asset and Expiration Date ('DDMMMYY" format) for DataGridView visualization
        private string _SelectedBaseAsset;
        private string _SelectedExpirationDate;

        public string SelectedBaseAsset
        {
            get { return _SelectedBaseAsset; }
            set
            { 
                _SelectedBaseAsset = value;
                if (_SelectedExpirationDate != null)
                {
                    UpdateRowMap(LinkedDataGridView, _SelectedBaseAsset, _SelectedExpirationDate);
                    NotifyPropertyChanged(nameof(SelectedBaseAsset));
                }
            }
        }
        public string SelectedExpirationDate
        {
            get { return _SelectedExpirationDate; }
            set
            {
                _SelectedExpirationDate = value;
                if (_SelectedBaseAsset != null)
                {
                    UpdateRowMap(LinkedDataGridView, _SelectedBaseAsset, _SelectedExpirationDate);
                    NotifyPropertyChanged(nameof(SelectedExpirationDate));
                }
            }
        }

        public DataGridView LinkedDataGridView { get; set; }
        public Dictionary<double, int> RowMap {get; set;} //Map for different strikes
        public Dictionary<string, int> ColumnMap = new Dictionary<string, int>()
        {
            { "CallRho", 0},
            { "CallTheta", 1},
            { "CallVega", 2},
            { "CallGamma", 3},
            { "CallDelta", 4},
            { "CallAskSize", 5},
            { "CallAskIV", 6},
            { "CallAskPrice", 7},
            { "CallBidPrice", 8},
            { "CallBidIV", 9},
            { "CallBidSize", 10},
            { "Strike", 11},
            { "PutBidSize", 12},
            { "PutBidIV", 13},
            { "PutBidPrice", 14},
            { "PutAskPrice", 15},
            { "PutAskIV", 16},
            { "PutAskSize", 17},
            { "PutDelta", 18},
            { "PutGamma", 19},
            { "PutVega", 20},
            { "PutTheta", 21},
            { "PutRho", 22}
        };


        public HashSet<double> Strikes { get; set; }
        public Dictionary<double, StrikeData> Series { get; set; } //Dict<strike, StrikeData>

        public double UnderlyingPrice { get; set; }


        public static void UpdateRowMap(DataGridView dataGridView, string base_currency, string expiration)
        {
            dataGridView.Rows.Clear();
            Dictionary<double, int> newRowMap = new Dictionary<double, int>();

            //Convert dates like "07APR23" to "7APR23"
            if (expiration.ToCharArray()[0] == '0')
                expiration = expiration.Remove(0, 1);

            var presentStrikes = CurrentBook.Data.Entries
                .Where(entry => (entry.Key.Contains(base_currency + "-" + expiration)))
                .OrderBy(a => a.Value.expiration_timestamp)
                .Select(a => a.Value.strike).ToList();

            //renew currently existing Strikes and Series
            Options.Data.Strikes = new HashSet<double>(presentStrikes);
            Options.Data.Series = new Dictionary<double, StrikeData>();

            int rowCount = 0;
            foreach (double strike in Options.Data.Strikes)
            {
                var strikeCallData = CurrentBook.Data.Entries[base_currency + "-" + expiration + "-" + strike.ToString() + "-C"];
                var strikePutData = CurrentBook.Data.Entries[base_currency + "-" + expiration + "-" + strike.ToString() + "-P"];

                Options.Data.Series.Add(strike,
                    new StrikeData()
                    {
                        CallAskPrice = strikeCallData.ask.Price,
                        CallAskSize = strikeCallData.ask.Quantity,
                        CallBidPrice = strikeCallData.bid.Price,
                        CallBidSize = strikeCallData.bid.Quantity,
                        PutBidPrice = strikePutData.bid.Price,
                        PutBidSize = strikePutData.bid.Quantity,
                        PutAskPrice = strikePutData.ask.Price,
                        PutAskSize = strikePutData.ask.Quantity,
                        Strike = strikePutData.strike,
                        ExpirationTimestamp = strikeCallData.expiration_timestamp,
                        LastTimestamp = Math.Max(strikeCallData.timestamp, strikePutData.timestamp)
                    });


                newRowMap[strike] = rowCount;
                dataGridView.Rows.Add();
                dataGridView[11, rowCount].Value = strike;
                rowCount++;
            }
            Options.Data.RowMap = newRowMap;
            foreach (var row in Options.Data.RowMap)
            {
                UpdateRow(row.Key);
            }
        }
        public static void UpdateGridCell(string colId, double strike, double value)
        {
            Options.Data.LinkedDataGridView[Options.Data.ColumnMap[colId], Options.Data.RowMap[strike]].Value = value;
        }
        public static void UpdateRow(double strike)
        {
            StrikeData strikeData = Options.Data.Series[strike];
            foreach (string columnName in Options.Data.ColumnMap.Keys)
            {
                try
                {
                    Options.Data.LinkedDataGridView[Options.Data.ColumnMap[columnName],
                        Options.Data.RowMap[strike]].Value = strikeData
                            .GetType()
                            .GetProperty(columnName)
                            .GetValue(strikeData, null);
                }
                catch
                { }
            }
        }
        public void UpdateValue(BookEntry updatedEntry)
        {
            if (updatedEntry.base_currency == Options.Data.SelectedBaseAsset &&
                DateTimeOffset.FromUnixTimeMilliseconds(updatedEntry.expiration_timestamp)
                .ToString("ddMMMyy").ToUpper() == Options.Data.SelectedExpirationDate)
            {
                if (updatedEntry.option_type == OptionType.call)
                {
                    Options.Data.Series[updatedEntry.strike].CallBidPrice = updatedEntry.bid.Price;
                    Options.Data.Series[updatedEntry.strike].CallAskPrice = updatedEntry.ask.Price;
                    Options.Data.Series[updatedEntry.strike].CallBidSize = updatedEntry.bid.Quantity;
                    Options.Data.Series[updatedEntry.strike].CallAskSize = updatedEntry.ask.Quantity;
                }
                if (updatedEntry.option_type == OptionType.put)
                {
                    Options.Data.Series[updatedEntry.strike].PutBidPrice = updatedEntry.bid.Price;
                    Options.Data.Series[updatedEntry.strike].PutAskPrice = updatedEntry.ask.Price;
                    Options.Data.Series[updatedEntry.strike].PutBidSize = updatedEntry.bid.Quantity;
                    Options.Data.Series[updatedEntry.strike].PutAskSize = updatedEntry.ask.Quantity;
                }
                Options.Data.Series[updatedEntry.strike].Strike = updatedEntry.strike;
                Options.Data.Series[updatedEntry.strike].ExpirationTimestamp = updatedEntry.expiration_timestamp;
                Options.Data.Series[updatedEntry.strike].LastTimestamp = updatedEntry.timestamp;

                //visualize update on DataGridView
                UpdateRow(updatedEntry.strike);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class StrikeData: INotifyPropertyChanged
    {
        private double riskFreeRate = 0;
        private double dividendYield = 0;

        private double _CallRho;
        private double _CallTheta;
        private double _CallVega;
        private double _CallGamma;
        private double _CallDelta;
        private double _CallAskSize;
        private double _CallAskIV;
        private double _CallAskPrice;
        private double _CallMidPrice;
        private double _CallBidPrice;
        private double _CallBidIV;
        private double _CallBidSize;
        private double _Strike;
        private double _PutBidSize;
        private double _PutBidIV;
        private double _PutBidPrice;
        private double _PutMidPrice;
        private double _PutAskPrice;
        private double _PutAskIV;
        private double _PutAskSize;
        private double _PutDelta;
        private double _PutGamma;
        private double _PutVega;
        private double _PutTheta;
        private double _PutRho;

        private long _ExpirationTimestamp;
        private long _LastTimestamp;


        public double CallRho
        {
            get { return _CallRho; }
            set
            {
                if (_CallRho != value)
                {
                    _CallRho = value;
                    NotifyPropertyChanged(nameof(CallRho));
                }
            }
        }

        public double CallTheta
        {
            get { return _CallTheta; }
            set
            {
                if (_CallTheta != value)
                {
                    _CallTheta = value;
                    NotifyPropertyChanged(nameof(CallTheta));
                }
            }
        }

        public double CallVega
        {
            get { return _CallVega; }
            set
            {
                if (_CallVega != value)
                {
                    _CallVega = value;
                    NotifyPropertyChanged(nameof(CallVega));
                }
            }
        }

        public double CallGamma
        {
            get { return _CallGamma; }
            set
            {
                if (_CallGamma != value)
                {
                    _CallGamma = value;
                    NotifyPropertyChanged(nameof(CallGamma));
                }
            }
        }

        public double CallDelta
        {
            get { return _CallDelta; }
            set
            {
                if (_CallDelta != value)
                {
                    _CallDelta = value;
                    NotifyPropertyChanged(nameof(CallDelta));
                }
            }
        }

        public double CallAskSize
        {
            get { return _CallAskSize; }
            set
            {
                if (_CallAskSize != value)
                {
                    _CallAskSize = value;
                    NotifyPropertyChanged(nameof(CallAskSize));
                }
            }
        }

        public double CallAskIV //stored in percents
        {
            get { return _CallAskIV; }
            private set
            {
                if (_CallAskIV != value)
                {
                    _CallAskIV = value;
                    NotifyPropertyChanged(nameof(CallAskIV));
                }
            }
        }

        public double CallAskPrice
        {
            get { return _CallAskPrice; }
            set
            {
                if (_CallAskPrice != value)
                {
                    _CallAskPrice = value;
                    NotifyPropertyChanged(nameof(CallAskPrice));
                }
            }
        }

        public double CallMidPrice
        {
            get { return _CallMidPrice; }
            private set
            {
                if (_CallMidPrice != value)
                {
                    _CallMidPrice = value;
                    NotifyPropertyChanged(nameof(CallMidPrice));
                }
            }
        }

        public double CallBidPrice
        {
            get { return _CallBidPrice; }
            set
            {
                if (_CallBidPrice != value)
                {
                    _CallBidPrice = value;
                    NotifyPropertyChanged(nameof(CallBidPrice));
                }
            }
        }

        public double CallBidIV //stored in percents
        {
            get { return _CallBidIV; }
            private set
            {
                if (_CallBidIV != value)
                {
                    _CallBidIV = value;
                    NotifyPropertyChanged(nameof(CallBidIV));
                }
            }
        }

        public double CallBidSize
        {
            get { return _CallBidSize; }
            set
            {
                if (_CallBidSize != value)
                {
                    _CallBidSize = value;
                    NotifyPropertyChanged(nameof(CallBidSize));
                }
            }
        }

        public double Strike
        {
            get { return _Strike; }
            set
            {
                if (_Strike != value)
                {
                    _Strike = value;
                    NotifyPropertyChanged(nameof(CallBidPrice));
                }
            }
        }

        public double PutBidSize //stored in percents
        {
            get { return _PutBidSize; }
            set
            {
                if (_PutBidSize != value)
                {
                    _PutBidSize = value;
                    NotifyPropertyChanged(nameof(PutBidSize));
                }
            }
        }

        public double PutBidIV //stored in percents
        {
            get { return _PutBidIV; }
            private set
            {
                if (_PutBidIV != value)
                {
                    _PutBidIV = value;
                    NotifyPropertyChanged(nameof(PutBidIV));
                }
            }
        }

        public double PutBidPrice
        {
            get { return _PutBidPrice; }
            set
            {
                if (_PutBidPrice != value)
                {
                    _PutBidPrice = value;
                    NotifyPropertyChanged(nameof(PutBidPrice));
                }
            }
        }

        public double PutMidPrice
        {
            get { return _PutMidPrice; }
            private set
            {
                if (_PutMidPrice != value)
                {
                    _PutMidPrice = value;
                    NotifyPropertyChanged(nameof(PutMidPrice));
                }
            }
        }

        public double PutAskPrice
        {
            get { return _PutAskPrice; }
            set
            {
                if (_PutAskPrice != value)
                {
                    _PutAskPrice = value;
                    NotifyPropertyChanged(nameof(PutAskPrice));
                }
            }
        }

        public double PutAskIV //stored in percents
        {
            get { return _PutAskIV; }
            private set
            {
                if (_PutAskIV != value)
                {
                    _PutAskIV = value;
                    NotifyPropertyChanged(nameof(PutAskIV));
                }
            }
        }

        public double PutAskSize //stored in percents
        {
            get { return _PutAskSize; }
            set
            {
                if (_PutAskSize != value)
                {
                    _PutAskSize = value;
                    NotifyPropertyChanged(nameof(PutAskSize));
                }
            }
        }

        public double PutDelta
        {
            get { return _PutDelta; }
            set
            {
                if (_PutDelta != value)
                {
                    _PutDelta = value;
                    NotifyPropertyChanged(nameof(PutDelta));
                }
            }
        }

        public double PutGamma
        {
            get { return _PutGamma; }
            set
            {
                if (_PutGamma != value)
                {
                    _PutGamma = value;
                    NotifyPropertyChanged(nameof(PutGamma));
                }
            }
        }

        public double PutVega
        {
            get { return _PutVega; }
            set
            {
                if (_PutVega != value)
                {
                    _PutVega = value;
                    NotifyPropertyChanged(nameof(PutVega));
                }
            }
        }

        public double PutTheta
        {
            get { return _PutTheta; }
            set
            {
                if (_PutTheta != value)
                {
                    _PutTheta = value;
                    NotifyPropertyChanged(nameof(PutTheta));
                }
            }
        }

        public double PutRho
        {
            get { return _PutRho; }
            set
            {
                if (_PutRho != value)
                {
                    _PutRho = value;
                    NotifyPropertyChanged(nameof(PutRho));
                }
            }
        }

        public long ExpirationTimestamp
        {
            get { return _ExpirationTimestamp; }
            set
            {
                if (_ExpirationTimestamp != value)
                {
                    _ExpirationTimestamp = value;
                    NotifyPropertyChanged(nameof(ExpirationTimestamp));
                }
            }
        }

        public long LastTimestamp
        {
            get { return _LastTimestamp; }
            set
            {
                if (_LastTimestamp != value)
                {
                    _LastTimestamp = value;
                    CalculateGreeks();
                    NotifyPropertyChanged(nameof(LastTimestamp));
                }
            }
        }

        //Calculation of Black-Scholes implied volatility and greeks
        private void CalculateGreeks()
        {
            _CallMidPrice = (_CallAskPrice + _CallBidPrice) / 2;
            _PutMidPrice = (_PutAskPrice + _PutBidPrice) / 2;

            double yearsToExpiry = (_ExpirationTimestamp - _LastTimestamp) / (double)31556925216;

            //CallAskIV
            if (_CallAskPrice != 0)
            {
                double IV = Math.Min(BlackScholes.BlackScholesImpliedVol(
                    _CallAskPrice * CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    riskFreeRate,
                    dividendYield,
                    OptionType.call
                    ) * 100, 999); //convert to percents and set max possible value to 999.0%

                if (Double.IsNaN(IV))
                    _CallAskIV = 0;
                else
                    _CallAskIV = IV;
            }
            else
            {
                _CallAskIV = 0;
            }

            //CallBidIV
            if (_CallBidPrice != 0)
            {
                double IV = Math.Min(BlackScholes.BlackScholesImpliedVol(
                    _CallBidPrice * CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    riskFreeRate,
                    dividendYield,
                    OptionType.call
                    ) * 100, 999); //convert to percents and set max possible value to 999.0%

                if (Double.IsNaN(IV))
                    _CallBidIV = 0;
                else
                    _CallBidIV = IV;
            }
            else
            {
                _CallBidIV = 0;
            }

            //PutAskIV
            if (_PutAskPrice != 0)
            {
                double IV = Math.Min(BlackScholes.BlackScholesImpliedVol(
                    _PutAskPrice * CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    riskFreeRate,
                    dividendYield,
                    OptionType.put
                    ) * 100, 999); //convert to percents and set max possible value to 999.0%

                if (Double.IsNaN(IV))
                    _PutAskIV = 0;
                else
                    _PutAskIV = IV;
            }
            else
            {
                _PutAskIV = 0;
            }

            //PutBidIV
            if (_PutBidPrice != 0)
            {
                double IV = Math.Min(BlackScholes.BlackScholesImpliedVol(
                    _PutBidPrice * CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    riskFreeRate,
                    dividendYield,
                    OptionType.put
                    ) * 100, 999); //convert to percents and set max possible value to 999.0%

                if (Double.IsNaN(IV))
                    _PutBidIV = 0;
                else
                    _PutBidIV = IV;
            }
            else
            {
                _PutBidIV = 0;
            }

            //Delta, Gamma, Vega, Theta, Rho for Call
            double callMidIV = (_CallAskIV + _CallBidIV) / 200; //convert back from percents
            if (callMidIV != 0)
            {
                double delta;
                double gamma;
                double vega;
                double theta;
                double rho;
                double convexity;

                BlackScholes.BlackScholesGreeks(
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    callMidIV,
                    riskFreeRate,
                    dividendYield,
                    OptionType.call,
                    out delta,
                    out gamma,
                    out vega,
                    out theta,
                    out rho,
                    out convexity
                    );

                _CallDelta = delta;
                _CallGamma = gamma;
                _CallVega = vega / 100;
                _CallTheta = theta / 365.2422;
                _CallRho = rho / 100;
            }

            //Delta, Gamma, Vega, Theta, Rho for Put
            double putMidIV = (_PutAskIV + _PutBidIV) / 200; //convert back from percents
            if (putMidIV != 0)
            {
                double delta;
                double gamma;
                double vega;
                double theta;
                double rho;
                double convexity;

                BlackScholes.BlackScholesGreeks(
                    _Strike,
                    CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset],
                    yearsToExpiry,
                    putMidIV,
                    riskFreeRate,
                    dividendYield,
                    OptionType.put,
                    out delta,
                    out gamma,
                    out vega,
                    out theta,
                    out rho,
                    out convexity
                    );

                _PutDelta = delta;
                _PutGamma = gamma;
                _PutVega = vega / 100;
                _PutTheta = theta / 365.2422;
                _PutRho = rho / 100;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
