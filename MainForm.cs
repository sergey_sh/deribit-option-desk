using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Deribit;
using Deribit.Objects;
using Deribit.Database;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;
using System.Drawing.Text;
using System.ComponentModel;
using System.Windows.Forms;
using Npgsql;
using System.Text.RegularExpressions;

namespace deribit_option_desk
{

    public delegate void ConnectDelegate();

    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            //GUI DataGrid settings
            Options.Data.LinkedDataGridView = dataGridView;
            dataGridView.Columns.Cast<DataGridViewColumn>()
                .ToList().ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);
            dataGridView.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Transparent;

            dataGridView.Columns[0].DefaultCellStyle.Format = "#0.00"; //CallRho
            dataGridView.Columns[1].DefaultCellStyle.Format = "#0.00"; //CallTheta
            dataGridView.Columns[2].DefaultCellStyle.Format = "#0.00"; //CallVega
            dataGridView.Columns[3].DefaultCellStyle.Format = "#0.000000"; //CallGamma
            dataGridView.Columns[4].DefaultCellStyle.Format = "#0.00"; //CallDelta
            dataGridView.Columns[5].DefaultCellStyle.Format = "#0.0"; //CallAskSize
            dataGridView.Columns[6].DefaultCellStyle.Format = "#0.0\\%"; //CallAskIV
            dataGridView.Columns[7].DefaultCellStyle.Format = "#0.0000"; //CallAskPrice
            dataGridView.Columns[8].DefaultCellStyle.Format = "#0.0000"; //CallBidPrice
            dataGridView.Columns[9].DefaultCellStyle.Format = "#0.0\\%"; //CallBidIV
            dataGridView.Columns[10].DefaultCellStyle.Format = "#0.0"; //CallBidSize
            dataGridView.Columns[12].DefaultCellStyle.Format = "#0.0"; //PutBidSize
            dataGridView.Columns[13].DefaultCellStyle.Format = "#0.0\\%"; //PutBidIV
            dataGridView.Columns[14].DefaultCellStyle.Format = "#0.0000"; //PutBidPrice
            dataGridView.Columns[15].DefaultCellStyle.Format = "#0.0000"; //PutAskPrice
            dataGridView.Columns[16].DefaultCellStyle.Format = "#0.0\\%"; //PutAskIV
            dataGridView.Columns[17].DefaultCellStyle.Format = "#0.0"; //PutAskSize
            dataGridView.Columns[18].DefaultCellStyle.Format = "#0.00"; //PutDelta
            dataGridView.Columns[19].DefaultCellStyle.Format = "#0.000000"; //PutGamma
            dataGridView.Columns[20].DefaultCellStyle.Format = "#0.00"; //PutVega
            dataGridView.Columns[21].DefaultCellStyle.Format = "#0.00"; //PutTheta
            dataGridView.Columns[22].DefaultCellStyle.Format = "#0.00"; //PutRho

            dataGridView.CellFormatting += DataGridView_CellFormatting; //Format DataGrid cells containing 0 as empty
        }



        //call the connection window
        private void ConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectForm connectForm = new ConnectForm();
            connectForm.Show();
            connectForm.StartConnection += GetDataAsync;
        }

        //establish connection
        // TODO: Add method for updating available expiration dates on event of issuing the ones by Deribit
        private async void GetDataAsync(object sender, EventArgs e)
        {
            //get all list of available options
            List<Instrument> myInstrumentList = new List<Instrument>();
            var instrumentData = await DeribitClient.GetInstruments();
            myInstrumentList = instrumentData.Item1; //list of available instruments
            long timestamp = instrumentData.Item2; //server timestamp for the data

            HashSet<long> expirationDatesSet = new HashSet<long>();

            //add the instrument to database (with internal check if it's already present)
            if (Database.isActive)
            {
                Database.CurrentDatabase.Connect();
                Database.CurrentDatabase.AddInstruments(myInstrumentList);
            }

            //initialize CurrentBook snapshot for options and add all expiration dates
            foreach (var instrument in myInstrumentList)
            {
                if (instrument.kind == "option")
                {
                    expirationDatesSet.Add(instrument.expiration_timestamp);

                    CurrentBook.Data.Entries.Add(
                        instrument.instrument_name,
                        new BookEntry
                        {
                            base_currency = instrument.base_currency,
                            timestamp = timestamp,
                            expiration_timestamp = instrument.expiration_timestamp,
                            strike = instrument.strike,
                            option_type = (OptionType)Enum.Parse(typeof(OptionType), instrument.option_type),
                            bid = new Bid { Price = 0, Quantity = 0 },
                            ask = new Ask { Price = 0, Quantity = 0 }
                        });
                }
            }
            CurrentBook.Data.AvailableExpirationDates = expirationDatesSet
                .OrderBy(x => x)
                .Select(x => DateTimeOffset.FromUnixTimeMilliseconds(x).ToString("ddMMMyy").ToUpper())
                .ToList();

            underlyingSelectComboBox.Items.Add("BTC");
            underlyingSelectComboBox.Items.Add("ETH");
            underlyingSelectComboBox.SelectedItem = "BTC";

            foreach (var date in CurrentBook.Data.AvailableExpirationDates)
            {
                expirationSelectComboBox.Items.Add(date);
            }
            expirationSelectComboBox.SelectedItem = CurrentBook.Data.AvailableExpirationDates.First();


            var uri = new Uri("wss://" + ConnectSettings.CurrentConnectSettings.serverAddress + "/ws/api/v2");
            using (var socket = new ClientWebSocket())
            {
                await socket.ConnectAsync(uri, CancellationToken.None);

                // Set up 3 connections: BTC options, ETH options and BTC/ETH underlying
                //(separate because otherwise subscribeMessage is too long for Deribit server)

                // BTC and ETH underlying connection
                List<string> channelListUnderlying = new List<string>()
                {
                    string.Join('.', new[] {"book", "BTC-PERPETUAL", "none", "1", "100ms"}),
                    string.Join('.', new[] {"book", "ETH-PERPETUAL", "none", "1", "100ms"})
                };
                SubscribeForChannelRange(socket, channelListUnderlying, 1);

                //BTC options connection
                List<string> channelListOptionsBTC = new List<string>();
                foreach (var instrument in myInstrumentList
                    .Where(a => a.base_currency == "BTC" && a.kind == "option").ToList())
                {
                    channelListOptionsBTC.Add(string.Join('.', new[] {"book",
                                                            instrument.instrument_name,
                                                            "none",
                                                            "1",
                                                            "100ms"}));
                }

                SubscribeForChannelRange(socket, channelListOptionsBTC, 2);

                //ETH options connection
                List<string> channelListOptionsETH = new List<string>();
                foreach (var instrument in myInstrumentList
                    .Where(a => a.base_currency == "ETH" && a.kind == "option").ToList())
                {
                    channelListOptionsETH.Add(string.Join('.', new[] {"book",
                                                            instrument.instrument_name,
                                                            "none",
                                                            "1",
                                                            "100ms"}));
                }
                SubscribeForChannelRange(socket, channelListOptionsETH, 3);

                // Receive messages
                while (socket.State == WebSocketState.Open)
                {
                    var buffer = new byte[1024 * 32];
                    var result = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        var message = Encoding.UTF8.GetString(buffer, 0, result.Count);
                        var json = JObject.Parse(message);
                        if (json["method"] != null && json["method"].ToString() == "subscription")
                        {
                            // Handle the order book update
                            string instrument_name = (string)json["params"]["data"]["instrument_name"];
                            long msg_timestamp = (long)json["params"]["data"]["timestamp"];
                            double bidPrice = 0;
                            double askPrice = 0;
                            double bidQuantity = 0;
                            double askQuantity = 0;

                            if (json["params"]["data"]["bids"].Count() > 0)
                            {
                                bidPrice = (double)json["params"]["data"]["bids"][0][0];
                                bidQuantity = (double)json["params"]["data"]["bids"][0][1];
                            }
                            if (json["params"]["data"]["asks"].Count() > 0)
                            {
                                askPrice = (double)json["params"]["data"]["asks"][0][0];
                                askQuantity = (double)json["params"]["data"]["asks"][0][1];
                            }

                            //add market data update to database
                            if (Database.isActive)
                            {
                                Database.CurrentDatabase.AddTickUpdate(msg_timestamp, instrument_name, bidPrice, askPrice);
                            }

                            //update the book snapshot
                            string pattern = @"^BTC-\d{1,2}[A-Z]{3}\d{2}-\d{5}-[CP]$";
                            if (Regex.IsMatch(instrument_name, pattern))
                            {
                                var entryToUpdate = CurrentBook.Data.Entries[instrument_name];
                                entryToUpdate.timestamp = msg_timestamp;
                                entryToUpdate.bid.Price = bidPrice;
                                entryToUpdate.bid.Quantity = bidQuantity;
                                entryToUpdate.ask.Price = askPrice;
                                entryToUpdate.ask.Quantity = askQuantity;

                                //update DataGridView
                                Options.Data.UpdateValue(entryToUpdate);
                            }
                            if (instrument_name.Contains("PERPETUAL"))
                            {
                                string assetName = instrument_name.Split('-')[0];
                                if (!CurrentBook.Data.UnderlyingPrices.ContainsKey(assetName) ||
                                    (bidPrice + askPrice) / 2 != CurrentBook.Data.UnderlyingPrices[assetName])
                                {
                                    CurrentBook.Data.UnderlyingPrices[assetName] = (bidPrice + askPrice) / 2;
                                }
                                if (Options.Data.SelectedBaseAsset == assetName)
                                    underlyingAssetPriceLabel.Text = Math.Round(CurrentBook.Data.UnderlyingPrices[assetName], 2).ToString();
                            }
                        }
                    }
                    else if (result.MessageType == WebSocketMessageType.Close)
                    {
                        await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                    }
                }
            }
        }

        public async void SubscribeForChannelRange(ClientWebSocket socket, List<string> channelList, int subscriptionId)
        {
            var subscribeMessage = new
            {
                jsonrpc = "2.0",
                id = subscriptionId,
                method = "public/subscribe",
                @params = new
                {
                    channels = channelList
                }
            };
            var messageJson = JsonConvert.SerializeObject(subscribeMessage);
            var messageBytes = Encoding.UTF8.GetBytes(messageJson);
            await socket.SendAsync(
                new ArraySegment<byte>(messageBytes),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None);
        }

        //Show volatility skew in a new window
        private void button1_Click(object sender, EventArgs e)
        {
            VolatilitySkewForm skewForm = new VolatilitySkewForm();
            skewForm.Show();
        }

        private void underlyingSelectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Options.Data.SelectedBaseAsset = underlyingSelectComboBox.SelectedItem.ToString();
        }

        private void expirationSelectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Options.Data.SelectedExpirationDate = expirationSelectComboBox.SelectedItem.ToString();
        }

        //Format DataGrid cells containing 0 as empty
        private void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //Format DataGrid cells containing 0 as empty
            if (e.Value is double value && value == 0)
            {
                e.Value = string.Empty;
                e.FormattingApplied = true;
            }

            //disable alternating color for strike column 7 8 14 15
            if (e.ColumnIndex == 11)
            {
                e.CellStyle.BackColor = ColorTranslator.FromHtml("#0000C0");
            }

            //Format DataGrid values for prices to USD if PricesFormat.FormatUsd == true
            if (Array.IndexOf(new int[] { 7, 8, 14, 15 }, (int)e.ColumnIndex) != -1
                && PricesFormat.FormatUsd == true
                && e.Value.GetType() == typeof(double))
            {
                e.Value = (double)e.Value * CurrentBook.Data.UnderlyingPrices[Options.Data.SelectedBaseAsset];
                e.CellStyle.Format = "$0.00";
            }
        }

        //Class to store price representation format
        public static class PricesFormat
        {
            public static bool FormatUsd = false;
        }

        private void priceStyleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            PricesFormat.FormatUsd = priceStyleCheckBox.Checked;
            Options.UpdateRowMap(
                Options.Data.LinkedDataGridView,
                Options.Data.SelectedBaseAsset,
                Options.Data.SelectedExpirationDate);
        }

        private void databaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseForm databaseForm = new DatabaseForm();
            databaseForm.Show();
            //ADD HANDLER ON CONNECT BUTTON
        }
    }
}