﻿using System.Windows.Forms;

namespace deribit_option_desk
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
            menuStrip1 = new MenuStrip();
            mainToolStripMenuItem = new ToolStripMenuItem();
            ConnectionToolStripMenuItem = new ToolStripMenuItem();
            preferencesToolStripMenuItem = new ToolStripMenuItem();
            databaseToolStripMenuItem = new ToolStripMenuItem();
            aboutToolStripMenuItem = new ToolStripMenuItem();
            underlyingSelectComboBox = new ComboBox();
            underlyingSelectLabel = new Label();
            expirationSelectLabel = new Label();
            expirationSelectComboBox = new ComboBox();
            dataGridView = new DataGridView();
            CallRho = new DataGridViewTextBoxColumn();
            CallTheta = new DataGridViewTextBoxColumn();
            CallVega = new DataGridViewTextBoxColumn();
            CallGamma = new DataGridViewTextBoxColumn();
            CallDelta = new DataGridViewTextBoxColumn();
            CallAskSize = new DataGridViewTextBoxColumn();
            CallAskIV = new DataGridViewTextBoxColumn();
            CallAskPrice = new DataGridViewTextBoxColumn();
            CallBidPrice = new DataGridViewTextBoxColumn();
            CallBidIV = new DataGridViewTextBoxColumn();
            CallBidSize = new DataGridViewTextBoxColumn();
            Strike = new DataGridViewTextBoxColumn();
            PutBidSize = new DataGridViewTextBoxColumn();
            PutBidIV = new DataGridViewTextBoxColumn();
            PutBidPrice = new DataGridViewTextBoxColumn();
            PutAskPrice = new DataGridViewTextBoxColumn();
            PutAskIV = new DataGridViewTextBoxColumn();
            PutAskSize = new DataGridViewTextBoxColumn();
            PutDelta = new DataGridViewTextBoxColumn();
            PutGamma = new DataGridViewTextBoxColumn();
            PutVega = new DataGridViewTextBoxColumn();
            PutTheta = new DataGridViewTextBoxColumn();
            PutRho = new DataGridViewTextBoxColumn();
            volatilitySkewButton = new Button();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            underlyingAssetPriceLabel = new Label();
            groupBox1 = new GroupBox();
            priceStyleCheckBox = new CheckBox();
            menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView).BeginInit();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.ImageScalingSize = new Size(20, 20);
            menuStrip1.Items.AddRange(new ToolStripItem[] { mainToolStripMenuItem, aboutToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1737, 28);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // mainToolStripMenuItem
            // 
            mainToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { ConnectionToolStripMenuItem, databaseToolStripMenuItem, preferencesToolStripMenuItem });
            mainToolStripMenuItem.Name = "mainToolStripMenuItem";
            mainToolStripMenuItem.Size = new Size(56, 24);
            mainToolStripMenuItem.Text = "Main";
            // 
            // ConnectionToolStripMenuItem
            // 
            ConnectionToolStripMenuItem.Name = "ConnectionToolStripMenuItem";
            ConnectionToolStripMenuItem.Size = new Size(224, 26);
            ConnectionToolStripMenuItem.Text = "Connection";
            ConnectionToolStripMenuItem.Click += ConnectionToolStripMenuItem_Click;
            // 
            // preferencesToolStripMenuItem
            // 
            preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            preferencesToolStripMenuItem.Size = new Size(224, 26);
            preferencesToolStripMenuItem.Text = "Preferences";
            // 
            // databaseToolStripMenuItem
            // 
            databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            databaseToolStripMenuItem.Size = new Size(224, 26);
            databaseToolStripMenuItem.Text = "Database";
            databaseToolStripMenuItem.Click += databaseToolStripMenuItem_Click;
            // 
            // aboutToolStripMenuItem
            // 
            aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            aboutToolStripMenuItem.Size = new Size(64, 24);
            aboutToolStripMenuItem.Text = "About";
            // 
            // underlyingSelectComboBox
            // 
            underlyingSelectComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            underlyingSelectComboBox.FormattingEnabled = true;
            underlyingSelectComboBox.Location = new Point(6, 61);
            underlyingSelectComboBox.Name = "underlyingSelectComboBox";
            underlyingSelectComboBox.Size = new Size(118, 28);
            underlyingSelectComboBox.TabIndex = 1;
            underlyingSelectComboBox.SelectedIndexChanged += underlyingSelectComboBox_SelectedIndexChanged;
            // 
            // underlyingSelectLabel
            // 
            underlyingSelectLabel.AutoSize = true;
            underlyingSelectLabel.Location = new Point(6, 38);
            underlyingSelectLabel.Name = "underlyingSelectLabel";
            underlyingSelectLabel.Size = new Size(118, 20);
            underlyingSelectLabel.TabIndex = 2;
            underlyingSelectLabel.Text = "Underlying asset";
            // 
            // expirationSelectLabel
            // 
            expirationSelectLabel.AutoSize = true;
            expirationSelectLabel.Location = new Point(6, 114);
            expirationSelectLabel.Name = "expirationSelectLabel";
            expirationSelectLabel.Size = new Size(76, 20);
            expirationSelectLabel.TabIndex = 4;
            expirationSelectLabel.Text = "Expiration";
            // 
            // expirationSelectComboBox
            // 
            expirationSelectComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            expirationSelectComboBox.FormattingEnabled = true;
            expirationSelectComboBox.Location = new Point(6, 137);
            expirationSelectComboBox.Name = "expirationSelectComboBox";
            expirationSelectComboBox.Size = new Size(118, 28);
            expirationSelectComboBox.TabIndex = 3;
            expirationSelectComboBox.SelectedIndexChanged += expirationSelectComboBox_SelectedIndexChanged;
            // 
            // dataGridView
            // 
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = Color.WhiteSmoke;
            dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridView.BackgroundColor = SystemColors.Control;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = SystemColors.Control;
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView.Columns.AddRange(new DataGridViewColumn[] { CallRho, CallTheta, CallVega, CallGamma, CallDelta, CallAskSize, CallAskIV, CallAskPrice, CallBidPrice, CallBidIV, CallBidSize, Strike, PutBidSize, PutBidIV, PutBidPrice, PutAskPrice, PutAskIV, PutAskSize, PutDelta, PutGamma, PutVega, PutTheta, PutRho });
            dataGridView.EditMode = DataGridViewEditMode.EditProgrammatically;
            dataGridView.GridColor = Color.White;
            dataGridView.Location = new Point(173, 59);
            dataGridView.Name = "dataGridView";
            dataGridView.ReadOnly = true;
            dataGridView.RowHeadersVisible = false;
            dataGridView.RowHeadersWidth = 51;
            dataGridView.RowTemplate.Height = 29;
            dataGridView.Size = new Size(1552, 776);
            dataGridView.TabIndex = 5;
            // 
            // CallRho
            // 
            CallRho.HeaderText = "Rho";
            CallRho.MinimumWidth = 6;
            CallRho.Name = "CallRho";
            CallRho.ReadOnly = true;
            CallRho.Width = 65;
            // 
            // CallTheta
            // 
            CallTheta.HeaderText = "Theta";
            CallTheta.MinimumWidth = 6;
            CallTheta.Name = "CallTheta";
            CallTheta.ReadOnly = true;
            CallTheta.Width = 70;
            // 
            // CallVega
            // 
            CallVega.HeaderText = "Vega";
            CallVega.MinimumWidth = 6;
            CallVega.Name = "CallVega";
            CallVega.ReadOnly = true;
            CallVega.Width = 50;
            // 
            // CallGamma
            // 
            CallGamma.HeaderText = "Gamma";
            CallGamma.MinimumWidth = 6;
            CallGamma.Name = "CallGamma";
            CallGamma.ReadOnly = true;
            CallGamma.Width = 80;
            // 
            // CallDelta
            // 
            CallDelta.HeaderText = "Delta";
            CallDelta.MinimumWidth = 6;
            CallDelta.Name = "CallDelta";
            CallDelta.ReadOnly = true;
            CallDelta.Width = 60;
            // 
            // CallAskSize
            // 
            CallAskSize.HeaderText = "Size";
            CallAskSize.MinimumWidth = 6;
            CallAskSize.Name = "CallAskSize";
            CallAskSize.ReadOnly = true;
            CallAskSize.Width = 60;
            // 
            // CallAskIV
            // 
            CallAskIV.HeaderText = "IV (Ask)";
            CallAskIV.MinimumWidth = 6;
            CallAskIV.Name = "CallAskIV";
            CallAskIV.ReadOnly = true;
            CallAskIV.Width = 60;
            // 
            // CallAskPrice
            // 
            dataGridViewCellStyle3.ForeColor = Color.FromArgb(192, 0, 0);
            dataGridViewCellStyle3.SelectionForeColor = Color.FromArgb(192, 0, 0);
            CallAskPrice.DefaultCellStyle = dataGridViewCellStyle3;
            CallAskPrice.HeaderText = "Ask";
            CallAskPrice.MinimumWidth = 6;
            CallAskPrice.Name = "CallAskPrice";
            CallAskPrice.ReadOnly = true;
            CallAskPrice.Width = 80;
            // 
            // CallBidPrice
            // 
            dataGridViewCellStyle4.ForeColor = Color.DarkGreen;
            dataGridViewCellStyle4.SelectionForeColor = Color.DarkGreen;
            CallBidPrice.DefaultCellStyle = dataGridViewCellStyle4;
            CallBidPrice.HeaderText = "Bid";
            CallBidPrice.MinimumWidth = 6;
            CallBidPrice.Name = "CallBidPrice";
            CallBidPrice.ReadOnly = true;
            CallBidPrice.Width = 80;
            // 
            // CallBidIV
            // 
            CallBidIV.HeaderText = "IV (Bid)";
            CallBidIV.MinimumWidth = 6;
            CallBidIV.Name = "CallBidIV";
            CallBidIV.ReadOnly = true;
            CallBidIV.Width = 60;
            // 
            // CallBidSize
            // 
            CallBidSize.HeaderText = "Size";
            CallBidSize.MinimumWidth = 6;
            CallBidSize.Name = "CallBidSize";
            CallBidSize.ReadOnly = true;
            CallBidSize.Width = 60;
            // 
            // Strike
            // 
            Strike.DataPropertyName = "Strike";
            dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = Color.FromArgb(0, 0, 192);
            dataGridViewCellStyle5.Font = new Font("Microsoft Sans Serif", 7.8F, FontStyle.Bold, GraphicsUnit.Point);
            dataGridViewCellStyle5.ForeColor = Color.White;
            dataGridViewCellStyle5.NullValue = null;
            Strike.DefaultCellStyle = dataGridViewCellStyle5;
            Strike.HeaderText = "Strike";
            Strike.MinimumWidth = 6;
            Strike.Name = "Strike";
            Strike.ReadOnly = true;
            Strike.Width = 80;
            // 
            // PutBidSize
            // 
            PutBidSize.HeaderText = "Size";
            PutBidSize.MinimumWidth = 6;
            PutBidSize.Name = "PutBidSize";
            PutBidSize.ReadOnly = true;
            PutBidSize.Width = 60;
            // 
            // PutBidIV
            // 
            PutBidIV.HeaderText = "IV (Bid)";
            PutBidIV.MinimumWidth = 6;
            PutBidIV.Name = "PutBidIV";
            PutBidIV.ReadOnly = true;
            PutBidIV.Width = 60;
            // 
            // PutBidPrice
            // 
            dataGridViewCellStyle6.ForeColor = Color.DarkGreen;
            dataGridViewCellStyle6.SelectionForeColor = Color.DarkGreen;
            PutBidPrice.DefaultCellStyle = dataGridViewCellStyle6;
            PutBidPrice.HeaderText = "Bid";
            PutBidPrice.MinimumWidth = 6;
            PutBidPrice.Name = "PutBidPrice";
            PutBidPrice.ReadOnly = true;
            PutBidPrice.Width = 80;
            // 
            // PutAskPrice
            // 
            dataGridViewCellStyle7.ForeColor = Color.FromArgb(192, 0, 0);
            dataGridViewCellStyle7.SelectionForeColor = Color.FromArgb(192, 0, 0);
            PutAskPrice.DefaultCellStyle = dataGridViewCellStyle7;
            PutAskPrice.HeaderText = "Ask";
            PutAskPrice.MinimumWidth = 6;
            PutAskPrice.Name = "PutAskPrice";
            PutAskPrice.ReadOnly = true;
            PutAskPrice.Width = 80;
            // 
            // PutAskIV
            // 
            PutAskIV.HeaderText = "IV (Ask)";
            PutAskIV.MinimumWidth = 6;
            PutAskIV.Name = "PutAskIV";
            PutAskIV.ReadOnly = true;
            PutAskIV.Width = 60;
            // 
            // PutAskSize
            // 
            PutAskSize.HeaderText = "Size";
            PutAskSize.MinimumWidth = 6;
            PutAskSize.Name = "PutAskSize";
            PutAskSize.ReadOnly = true;
            PutAskSize.Width = 60;
            // 
            // PutDelta
            // 
            PutDelta.HeaderText = "Delta";
            PutDelta.MinimumWidth = 6;
            PutDelta.Name = "PutDelta";
            PutDelta.ReadOnly = true;
            PutDelta.Width = 60;
            // 
            // PutGamma
            // 
            PutGamma.HeaderText = "Gamma";
            PutGamma.MinimumWidth = 6;
            PutGamma.Name = "PutGamma";
            PutGamma.ReadOnly = true;
            PutGamma.Width = 80;
            // 
            // PutVega
            // 
            PutVega.HeaderText = "Vega";
            PutVega.MinimumWidth = 6;
            PutVega.Name = "PutVega";
            PutVega.ReadOnly = true;
            PutVega.Width = 50;
            // 
            // PutTheta
            // 
            PutTheta.HeaderText = "Theta";
            PutTheta.MinimumWidth = 6;
            PutTheta.Name = "PutTheta";
            PutTheta.ReadOnly = true;
            PutTheta.Width = 70;
            // 
            // PutRho
            // 
            PutRho.HeaderText = "Rho";
            PutRho.MinimumWidth = 6;
            PutRho.Name = "PutRho";
            PutRho.ReadOnly = true;
            PutRho.Width = 65;
            // 
            // volatilitySkewButton
            // 
            volatilitySkewButton.Location = new Point(7, 239);
            volatilitySkewButton.Name = "volatilitySkewButton";
            volatilitySkewButton.Size = new Size(118, 29);
            volatilitySkewButton.TabIndex = 6;
            volatilitySkewButton.Text = "Volatility skew";
            volatilitySkewButton.UseVisualStyleBackColor = true;
            volatilitySkewButton.Click += button1_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(173, 33);
            label1.Name = "label1";
            label1.Size = new Size(47, 23);
            label1.TabIndex = 7;
            label1.Text = "Calls";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(1641, 33);
            label2.Name = "label2";
            label2.Size = new Size(44, 23);
            label2.TabIndex = 8;
            label2.Text = "Puts";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = Color.LemonChiffon;
            label3.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(818, 35);
            label3.Name = "label3";
            label3.Size = new Size(171, 20);
            label3.TabIndex = 9;
            label3.Text = "Underlying Asset Price:";
            // 
            // underlyingAssetPriceLabel
            // 
            underlyingAssetPriceLabel.AutoSize = true;
            underlyingAssetPriceLabel.BackColor = Color.LemonChiffon;
            underlyingAssetPriceLabel.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            underlyingAssetPriceLabel.Location = new Point(995, 35);
            underlyingAssetPriceLabel.Name = "underlyingAssetPriceLabel";
            underlyingAssetPriceLabel.Size = new Size(18, 20);
            underlyingAssetPriceLabel.TabIndex = 10;
            underlyingAssetPriceLabel.Text = "0";
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(priceStyleCheckBox);
            groupBox1.Controls.Add(underlyingSelectComboBox);
            groupBox1.Controls.Add(underlyingSelectLabel);
            groupBox1.Controls.Add(expirationSelectComboBox);
            groupBox1.Controls.Add(expirationSelectLabel);
            groupBox1.Controls.Add(volatilitySkewButton);
            groupBox1.Location = new Point(6, 49);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(146, 351);
            groupBox1.TabIndex = 11;
            groupBox1.TabStop = false;
            groupBox1.Text = "Controls";
            // 
            // priceStyleCheckBox
            // 
            priceStyleCheckBox.AutoSize = true;
            priceStyleCheckBox.Location = new Point(7, 193);
            priceStyleCheckBox.Name = "priceStyleCheckBox";
            priceStyleCheckBox.Size = new Size(118, 24);
            priceStyleCheckBox.TabIndex = 7;
            priceStyleCheckBox.Text = "Prices in USD";
            priceStyleCheckBox.UseVisualStyleBackColor = true;
            priceStyleCheckBox.CheckedChanged += priceStyleCheckBox_CheckedChanged;
            // 
            // Main
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1737, 847);
            Controls.Add(underlyingAssetPriceLabel);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(dataGridView);
            Controls.Add(menuStrip1);
            Controls.Add(groupBox1);
            MainMenuStrip = menuStrip1;
            Name = "Main";
            Text = "Option Desk";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem mainToolStripMenuItem;
        private ToolStripMenuItem ConnectionToolStripMenuItem;
        private ToolStripMenuItem preferencesToolStripMenuItem;
        private ToolStripMenuItem databaseToolStripMenuItem;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private ComboBox underlyingSelectComboBox;
        private Label underlyingSelectLabel;
        private Label expirationSelectLabel;
        private ComboBox expirationSelectComboBox;
        private Button volatilitySkewButton;
        private Label label1;
        private Label label2;
        public DataGridView dataGridView;
        private Label label3;
        private Label underlyingAssetPriceLabel;
        private GroupBox groupBox1;
        private CheckBox priceStyleCheckBox;
        private DataGridViewTextBoxColumn CallRho;
        private DataGridViewTextBoxColumn CallTheta;
        private DataGridViewTextBoxColumn CallVega;
        private DataGridViewTextBoxColumn CallGamma;
        private DataGridViewTextBoxColumn CallDelta;
        private DataGridViewTextBoxColumn CallAskSize;
        private DataGridViewTextBoxColumn CallAskIV;
        private DataGridViewTextBoxColumn CallAskPrice;
        private DataGridViewTextBoxColumn CallBidPrice;
        private DataGridViewTextBoxColumn CallBidIV;
        private DataGridViewTextBoxColumn CallBidSize;
        private DataGridViewTextBoxColumn Strike;
        private DataGridViewTextBoxColumn PutBidSize;
        private DataGridViewTextBoxColumn PutBidIV;
        private DataGridViewTextBoxColumn PutBidPrice;
        private DataGridViewTextBoxColumn PutAskPrice;
        private DataGridViewTextBoxColumn PutAskIV;
        private DataGridViewTextBoxColumn PutAskSize;
        private DataGridViewTextBoxColumn PutDelta;
        private DataGridViewTextBoxColumn PutGamma;
        private DataGridViewTextBoxColumn PutVega;
        private DataGridViewTextBoxColumn PutTheta;
        private DataGridViewTextBoxColumn PutRho;
    }
}