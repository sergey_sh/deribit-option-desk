# Deribit Option Desk

A windows desktop application (.NET 6.0) for visualization Deribit�s option prices with interactive calculation of respective Greeks and volatility skew visualization, with implemented possibility of real-time recording of options� order book updates for to PostgreSQL database.

# Installation

1. If you don't have Visual Studio installed, download and install the latest version of Visual Studio from the official Microsoft website.

2. Clone the repository.

3. Open the solution in Solution View by double-clicking its .sln file

4. Check for and install necessary NuGet packages (Solution Explorer -> Manage NuGet Packages). The following packages are used in the project:
   - Newtonsoft.Json � version 13.0.3
   - Npgsql � version 7.0.2
   - Npgsql.EntityFrameworkCore.PostgreSQL � version 7.0.3
   - ScottPlot.WinForms � version 4.1.61

5. Compile/Build the solution.

# Usage

1. **Connecting to Deribit**
Proceed with Main -> Connection -> (set up required parameters) -> Connect. Currently implemented method employs JSON-RPC over Websocket. GUI allows interactive visualizing of option prices and greeks for option series with selected expiration date and underlying asset (time resolution of updates � 100 ms). An interactive volatility skew for the selected options series may be visualized in a separate window (updated every 5 seconds).

    Notes:

    - no internal check for possible inconsistency of timestamps (due to network jitter/latency variations) for arriving exchange messages is conducted, hence, in volatile market conditions the visualized prices/greeks may not exactly represent the real situation and may be inaccurately calculated;
    - risk free rate for greeks calculation is currently hardcoded to zero;
    - greeks are currently calculated by averaging available implied volatilities for respective ask and bid prices. In the future, Stochastic Volatility Inspired model is planned to be implemented.

3. **Collecting data to a PostgreSQL database**.
When launched, if you�d like to collect market data into a database to analyze it later on, set up database connection settings (Main -> Database -> (set your parameters) -> Connect) **before** connecting to Deribit. You must have an empty database, or the one which has already been used by this application. If the database is empty, the application will create the corresponding tables, otherwise it will continue writing to previously created and used ones. Two tables are maintained:

   - �instruments� � stores data for available instruments (tick_size, taker_commission, strike, settlement_period, settlement_currency, rfq, quote_currency, price_index, option_type, min_trade_amount, maker_commission, kind, is_active, instrument_name, instrument_id, expiration_timestamp, creation_timestamp, counter_currency, contract_size, block_trade_tick_size, block_trade_min_trade_amount, block_trade_commission, base_currency � refer to official Deribit API web site for information on these parameters);

   - �ticks� � data for market order book updates (timestamp, instrument_name, best bid price, best ask price) for all available option contracts and underlying assets.

# Examples
![image](./Examples/Main_window.png)
Main window with open windows for setting Deribit and database connections.
<br/><br/>

![image](./Examples/Volatility_skew.png)
Volatility skew.


# Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feel free to copy, use and modify for personal purposes.

# Contact

If you have any suggestions or would like to provide valuable feedback, please drop an email to [sergey.gitlab@gmail.com](mailto:sergey.gitlab@gmail.com) and make sure that the email�s subject is DERIBIT_OPTION_DESK_GITLAB.

# Acknowledments
http://www.risk256.com/