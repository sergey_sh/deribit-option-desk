﻿namespace deribit_option_desk
{
    partial class VolatilitySkewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            skewPlot = new ScottPlot.FormsPlot();
            optionTypeGroupBox = new GroupBox();
            selectPutRadioButton = new RadioButton();
            selectCallRadioButton = new RadioButton();
            optionTypeGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // skewPlot
            // 
            skewPlot.Location = new Point(4, 81);
            skewPlot.Margin = new Padding(5, 4, 5, 4);
            skewPlot.Name = "skewPlot";
            skewPlot.Size = new Size(1049, 763);
            skewPlot.TabIndex = 0;
            // 
            // optionTypeGroupBox
            // 
            optionTypeGroupBox.Controls.Add(selectPutRadioButton);
            optionTypeGroupBox.Controls.Add(selectCallRadioButton);
            optionTypeGroupBox.Location = new Point(55, 12);
            optionTypeGroupBox.Name = "optionTypeGroupBox";
            optionTypeGroupBox.Size = new Size(157, 62);
            optionTypeGroupBox.TabIndex = 1;
            optionTypeGroupBox.TabStop = false;
            optionTypeGroupBox.Text = "Option type";
            // 
            // selectPutRadioButton
            // 
            selectPutRadioButton.AutoSize = true;
            selectPutRadioButton.Location = new Point(89, 26);
            selectPutRadioButton.Name = "selectPutRadioButton";
            selectPutRadioButton.Size = new Size(51, 24);
            selectPutRadioButton.TabIndex = 2;
            selectPutRadioButton.TabStop = true;
            selectPutRadioButton.Text = "Put";
            selectPutRadioButton.UseVisualStyleBackColor = true;
            // 
            // selectCallRadioButton
            // 
            selectCallRadioButton.AutoSize = true;
            selectCallRadioButton.Location = new Point(11, 25);
            selectCallRadioButton.Name = "selectCallRadioButton";
            selectCallRadioButton.Size = new Size(55, 24);
            selectCallRadioButton.TabIndex = 0;
            selectCallRadioButton.TabStop = true;
            selectCallRadioButton.Text = "Call";
            selectCallRadioButton.UseVisualStyleBackColor = true;
            selectCallRadioButton.CheckedChanged += selectCallRadioButton_CheckedChanged;
            // 
            // VolatilitySkewForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1055, 847);
            Controls.Add(optionTypeGroupBox);
            Controls.Add(skewPlot);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "VolatilitySkewForm";
            optionTypeGroupBox.ResumeLayout(false);
            optionTypeGroupBox.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private ScottPlot.FormsPlot skewPlot;
        private GroupBox optionTypeGroupBox;
        private RadioButton selectPutRadioButton;
        private RadioButton selectCallRadioButton;
    }
}