﻿using Deribit.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottPlot;
using System.Diagnostics;
using ScottPlot.Control.EventProcess.Events;
using Microsoft.Win32;

namespace deribit_option_desk
{
    public partial class VolatilitySkewForm : Form
    {
        public VolatilitySkewForm()
        {
            InitializeComponent();
            selectCallRadioButton.Checked = true;
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "ask");
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "bid");
            Options.Data.PropertyChanged += OptionSeriesSelection_PropertyChanged;

            //update the skew every 5 sec - ObservableDictionary - another option for interactivity?
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5000;
            timer.Tick += UpdateSkew;
            timer.Start();
        }

        private void OptionSeriesSelection_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            skewPlot.Plot.Clear(); // Clear previous points
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "ask");
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "bid");
        }

        private void UpdateSkew(object sender, EventArgs e)
        {
            skewPlot.Plot.Clear(); // Clear previous points
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "ask");
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "bid");
        }

        private void DrawSkew(string baseAsset, string expirationDate, string side)
        {
            Color color;

            double[] dataX = Options.Data.Strikes.OrderBy(a => a).ToArray<double>();
            double[] dataY = new double[] { };

            if (selectCallRadioButton.Checked == true)
            {
                if (side == "ask")
                {
                    dataY = Options.Data.Series
                        .OrderBy(a => a.Value.Strike)
                        .Select(a => a.Value.CallAskIV)
                        .ToArray<double>();
                    color = Color.Red;
                }
                else
                {
                    dataY = Options.Data.Series
                        .OrderBy(a => a.Value.Strike)
                        .Select(a => a.Value.CallBidIV)
                        .ToArray<double>();
                    color = Color.Green;
                }
            }
            else
            {
                if (side == "ask")
                {
                    dataY = Options.Data.Series
                        .OrderBy(a => a.Value.Strike)
                        .Select(a => a.Value.PutAskIV)
                        .ToArray<double>();
                    color = Color.Red;
                }
                else
                {
                    dataY = Options.Data.Series
                        .OrderBy(a => a.Value.Strike)
                        .Select(a => a.Value.PutBidIV)
                        .ToArray<double>();
                    color = Color.Green;
                }
            }

            for (int i = 0; i < dataY.Length; i++) //replace zeros with NaN
            {
                if (dataY[i] == 0)
                    dataY[i] = double.NaN;
            }

            var plt = skewPlot.Plot.AddScatter(
                dataX,
                dataY,
                color,
                1,
                5,
                MarkerShape.filledCircle,
                LineStyle.None,
                "Ask IV");

            plt.OnNaN = ScottPlot.Plottable.ScatterPlot.NanBehavior.Ignore;

            skewPlot.Refresh();
        }

        private void selectCallRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            skewPlot.Plot.Clear(); // Clear previous points
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "ask");
            DrawSkew(Options.Data.SelectedBaseAsset, Options.Data.SelectedExpirationDate, "bid");
        }
    }
}
