﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Microsoft.EntityFrameworkCore;
using Deribit.Objects;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Linq.Expressions;

namespace Deribit.Database
{
    //class for database settings
    public class Database
    {
        private Database() { }

        private static readonly Lazy<Database> lazy =
            new Lazy<Database>(() => new Database());

        public static Database CurrentDatabase
        {
            get
            {
                return lazy.Value;
            }
            set
            {

            }
        }

        public static bool isActive { get; private set; }
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string DatabaseName { get; set; }

        private NpgsqlConnection Connection;

        //for tick batch updates
        private const int batchSize = 100;
        private int count = 0;
        private InstrumentDbContext tickContext = new InstrumentDbContext();

        private static string _connectionString;
        private List<int> _presentInstrumentIds;

        public async void Connect()
        {
            NpgsqlConnectionStringBuilder sb = new NpgsqlConnectionStringBuilder();

            sb.Host = Host;
            sb.Username = Username;
            sb.Password = Password;
            sb.Port = Port;
            sb.Database = DatabaseName;

            _connectionString = sb.ToString();

            Connection = new NpgsqlConnection(_connectionString);
            await Connection.OpenAsync();
            using (var db = new InstrumentDbContext())
            {
                db.Database.EnsureCreated();
                _presentInstrumentIds = db.instruments.Select(a => a.instrument_id).ToList();
            }

            isActive = true;
        }

        //updating instruments database table
        public class InstrumentDbContext : DbContext
        {
            public DbSet<Instrument> instruments { get; set; }
            public DbSet<TickData> ticks { get; set; }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Instrument>()
                    .HasKey(i => i.instrument_name);
                modelBuilder.Entity<TickData>()
                    .HasKey(t => new { t.instrument_name, t.timestamp });
                modelBuilder.Entity<TickData>()
                    .HasOne(t => t.Instrument)
                    .WithMany(i => i.Ticks)
                    .HasForeignKey(t => t.instrument_name);
            }
            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder.UseNpgsql(_connectionString);
                optionsBuilder.EnableSensitiveDataLogging();
            }

        }

        public async void AddInstruments(List<Instrument> instruments)
        {
            using (var db = new InstrumentDbContext())
            {
                foreach (var instrument in instruments)
                {
                    if (!_presentInstrumentIds.Contains(instrument.instrument_id))
                    {
                        if (instrument.option_type == null)
                            instrument.option_type = "NA";
                        await db.instruments.AddAsync(instrument);
                    }
                }
                await db.SaveChangesAsync();
            }
        }

        public class TickData
        {
            public DateTime timestamp { get; set; }
            public string instrument_name { get; set; }
            public double bid { get; set; }
            public double ask { get; set; }
            public Instrument Instrument { get; set; }
        }

        public void AddTickUpdate(long timestamp_ms, string instrument_name, double bid, double ask)
        {
            DateTime timestamp = DateTimeOffset.FromUnixTimeMilliseconds(timestamp_ms).UtcDateTime;
            TickData data = new TickData()
            {
                timestamp = timestamp,
                instrument_name = instrument_name,
                bid = bid,
                ask = ask
            };
            count++;
            if (count == batchSize)
            {
                count = 0;
                try
                {
                    tickContext.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    //at the moment, simply skip all if any duplicate found
                    tickContext.Set<TickData>().RemoveRange(tickContext.Set<TickData>());
                    tickContext.SaveChanges();
                }
            }

            //remove duplicates if present - SemaphoreSlim lock if async?
            try
            {
                tickContext.Add(data);
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.Contains("instance of entity type 'TickData' cannot be tracked because another instance with the key value"))
                {
                    var entries = tickContext.ChangeTracker.Entries<TickData>()
                        .Where(e => e.Entity.instrument_name == data.instrument_name
                        && e.Entity.timestamp == data.timestamp).ToList();
                    foreach (var entry in entries)
                    {
                        if (entry != null)
                        {
                            tickContext.Remove(entry.Entity);
                        }
                    }
                    tickContext.SaveChanges();
                }
            }
        }
    }
}
