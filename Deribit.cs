﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using deribit_option_desk;
using Deribit.Objects;

namespace Deribit
{
    
    public class DeribitClient
    {
        static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(unixTimeStamp);
            return dateTimeOffset.DateTime;
        }
        public static async Task<(List<Instrument>, long)> GetInstruments()
        {
            long timestamp = 0;
            HttpClient client = new HttpClient();
            
            string urlFormat = "https://" + 
                ConnectSettings.CurrentConnectSettings.serverAddress + 
                "/api/v2/public/get_instruments?currency={0}&expired=false&kind={1}";
            string[] currencies = new string[] { "BTC", "ETH" };
            string[] kinds = new string[] { "option", "future" };
            List<Instrument> allInstruments = new List<Instrument>();

            foreach (var currency in currencies)
            {
                foreach (var kind in kinds)
                {
                    var url = string.Format(urlFormat, currency, kind);
                    var instruments = await InstrumentsToArray(client, url);
                    allInstruments.AddRange(instruments.Item1);
                    timestamp = Math.Max(timestamp, (long)(instruments.Item2 / 1000)); //usOut timestamp im ms
                }
            }
            return (allInstruments, timestamp);
        }
        public static async Task<(List<Instrument>, long)> InstrumentsToArray(HttpClient client, string url)
        {
            var response = await client.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            InstrumentsResponse instrumentsResponse = JsonConvert.DeserializeObject<InstrumentsResponse>(json);
            List<Instrument> symbols = instrumentsResponse.result;
            long timestamp = instrumentsResponse.usOut; //in microseconds
            return (symbols, timestamp);
        }
    }
}
