﻿namespace deribit_option_desk
{
    partial class DatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            hostLabel = new Label();
            PortLabel = new Label();
            hostTextBox = new TextBox();
            portTextBox = new TextBox();
            databaseNameTextBox = new TextBox();
            databaseNameLabel = new Label();
            usernameTextBox = new TextBox();
            usernameLabel = new Label();
            passwordTextBox = new TextBox();
            passwordLabel = new Label();
            loadDefaultSettingsButton = new Button();
            saveDefaultSettingsButton = new Button();
            databaseConnectButton = new Button();
            SuspendLayout();
            // 
            // hostLabel
            // 
            hostLabel.AutoSize = true;
            hostLabel.Location = new Point(21, 24);
            hostLabel.Name = "hostLabel";
            hostLabel.Size = new Size(40, 20);
            hostLabel.TabIndex = 0;
            hostLabel.Text = "Host";
            // 
            // PortLabel
            // 
            PortLabel.AutoSize = true;
            PortLabel.Location = new Point(21, 57);
            PortLabel.Name = "PortLabel";
            PortLabel.Size = new Size(35, 20);
            PortLabel.TabIndex = 1;
            PortLabel.Text = "Port";
            // 
            // hostTextBox
            // 
            hostTextBox.Location = new Point(148, 21);
            hostTextBox.Name = "hostTextBox";
            hostTextBox.Size = new Size(125, 27);
            hostTextBox.TabIndex = 2;
            hostTextBox.TextChanged += hostTextBox_TextChanged;
            // 
            // portTextBox
            // 
            portTextBox.Location = new Point(148, 54);
            portTextBox.Name = "portTextBox";
            portTextBox.Size = new Size(125, 27);
            portTextBox.TabIndex = 3;
            portTextBox.TextChanged += portTextBox_TextChanged;
            // 
            // databaseNameTextBox
            // 
            databaseNameTextBox.Location = new Point(148, 87);
            databaseNameTextBox.Name = "databaseNameTextBox";
            databaseNameTextBox.Size = new Size(125, 27);
            databaseNameTextBox.TabIndex = 4;
            databaseNameTextBox.TextChanged += databaseNameTextBox_TextChanged;
            // 
            // databaseNameLabel
            // 
            databaseNameLabel.AutoSize = true;
            databaseNameLabel.Location = new Point(21, 90);
            databaseNameLabel.Name = "databaseNameLabel";
            databaseNameLabel.Size = new Size(113, 20);
            databaseNameLabel.TabIndex = 5;
            databaseNameLabel.Text = "Database name";
            // 
            // usernameTextBox
            // 
            usernameTextBox.Location = new Point(148, 120);
            usernameTextBox.Name = "usernameTextBox";
            usernameTextBox.Size = new Size(125, 27);
            usernameTextBox.TabIndex = 6;
            usernameTextBox.TextChanged += usernameTextBox_TextChanged;
            // 
            // usernameLabel
            // 
            usernameLabel.AutoSize = true;
            usernameLabel.Location = new Point(21, 123);
            usernameLabel.Name = "usernameLabel";
            usernameLabel.Size = new Size(75, 20);
            usernameLabel.TabIndex = 7;
            usernameLabel.Text = "Username";
            // 
            // passwordTextBox
            // 
            passwordTextBox.Location = new Point(148, 153);
            passwordTextBox.Name = "passwordTextBox";
            passwordTextBox.Size = new Size(125, 27);
            passwordTextBox.TabIndex = 8;
            passwordTextBox.UseSystemPasswordChar = true;
            passwordTextBox.TextChanged += passwordTextBox_TextChanged;
            // 
            // passwordLabel
            // 
            passwordLabel.AutoSize = true;
            passwordLabel.Location = new Point(21, 156);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new Size(70, 20);
            passwordLabel.TabIndex = 9;
            passwordLabel.Text = "Password";
            // 
            // loadDefaultSettingsButton
            // 
            loadDefaultSettingsButton.Location = new Point(21, 208);
            loadDefaultSettingsButton.Name = "loadDefaultSettingsButton";
            loadDefaultSettingsButton.Size = new Size(157, 29);
            loadDefaultSettingsButton.TabIndex = 10;
            loadDefaultSettingsButton.Text = "Load default settings";
            loadDefaultSettingsButton.UseVisualStyleBackColor = true;
            loadDefaultSettingsButton.Click += loadDefaultSettingsButton_Click;
            // 
            // saveDefaultSettingsButton
            // 
            saveDefaultSettingsButton.Location = new Point(196, 208);
            saveDefaultSettingsButton.Name = "saveDefaultSettingsButton";
            saveDefaultSettingsButton.Size = new Size(157, 29);
            saveDefaultSettingsButton.TabIndex = 11;
            saveDefaultSettingsButton.Text = "Save as default";
            saveDefaultSettingsButton.UseVisualStyleBackColor = true;
            saveDefaultSettingsButton.Click += saveDefaultSettingsButton_Click;
            // 
            // databaseConnectButton
            // 
            databaseConnectButton.Location = new Point(259, 262);
            databaseConnectButton.Name = "databaseConnectButton";
            databaseConnectButton.Size = new Size(94, 29);
            databaseConnectButton.TabIndex = 12;
            databaseConnectButton.Text = "Connect";
            databaseConnectButton.UseVisualStyleBackColor = true;
            databaseConnectButton.Click += databaseConnectButton_Click;
            // 
            // DatabaseForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(376, 317);
            Controls.Add(databaseConnectButton);
            Controls.Add(saveDefaultSettingsButton);
            Controls.Add(loadDefaultSettingsButton);
            Controls.Add(passwordLabel);
            Controls.Add(passwordTextBox);
            Controls.Add(usernameLabel);
            Controls.Add(usernameTextBox);
            Controls.Add(databaseNameLabel);
            Controls.Add(databaseNameTextBox);
            Controls.Add(portTextBox);
            Controls.Add(hostTextBox);
            Controls.Add(PortLabel);
            Controls.Add(hostLabel);
            Name = "DatabaseForm";
            Text = "Database settings";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label hostLabel;
        private Label PortLabel;
        private TextBox hostTextBox;
        private TextBox portTextBox;
        private TextBox databaseNameTextBox;
        private Label databaseNameLabel;
        private TextBox usernameTextBox;
        private Label usernameLabel;
        private TextBox passwordTextBox;
        private Label passwordLabel;
        private Button loadDefaultSettingsButton;
        private Button saveDefaultSettingsButton;
        private Button databaseConnectButton;
    }
}