﻿namespace deribit_option_desk
{
    partial class ConnectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            apiInterfaceLabel = new Label();
            apiInterfaceComboBox = new ComboBox();
            serverAddressLabel = new Label();
            serverAddressComboBox = new ComboBox();
            clientIdLabel = new Label();
            clientSecretLabel = new Label();
            clientIdTextBox = new TextBox();
            clientSecretTextBox = new TextBox();
            connectButton = new Button();
            loadDefaultConnectSettingsButton = new Button();
            saveConnectSettingsButton = new Button();
            SuspendLayout();
            // 
            // apiInterfaceLabel
            // 
            apiInterfaceLabel.AutoSize = true;
            apiInterfaceLabel.Location = new Point(12, 19);
            apiInterfaceLabel.Name = "apiInterfaceLabel";
            apiInterfaceLabel.RightToLeft = RightToLeft.No;
            apiInterfaceLabel.Size = new Size(93, 20);
            apiInterfaceLabel.TabIndex = 0;
            apiInterfaceLabel.Text = "API interface";
            // 
            // apiInterfaceComboBox
            // 
            apiInterfaceComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            apiInterfaceComboBox.FormattingEnabled = true;
            apiInterfaceComboBox.Items.AddRange(new object[] { "FIX API", "JSON-RPC websocket" });
            apiInterfaceComboBox.Location = new Point(135, 16);
            apiInterfaceComboBox.Name = "apiInterfaceComboBox";
            apiInterfaceComboBox.Size = new Size(151, 28);
            apiInterfaceComboBox.TabIndex = 1;
            apiInterfaceComboBox.SelectedIndexChanged += apiInterfaceComboBox_SelectedIndexChanged;
            // 
            // serverAddressLabel
            // 
            serverAddressLabel.AutoSize = true;
            serverAddressLabel.Location = new Point(12, 63);
            serverAddressLabel.Name = "serverAddressLabel";
            serverAddressLabel.RightToLeft = RightToLeft.No;
            serverAddressLabel.Size = new Size(105, 20);
            serverAddressLabel.TabIndex = 2;
            serverAddressLabel.Text = "Server address";
            // 
            // serverAddressComboBox
            // 
            serverAddressComboBox.FormattingEnabled = true;
            serverAddressComboBox.Items.AddRange(new object[] { "test.deribit.com", "www.deribit.com" });
            serverAddressComboBox.Location = new Point(135, 60);
            serverAddressComboBox.Name = "serverAddressComboBox";
            serverAddressComboBox.Size = new Size(151, 28);
            serverAddressComboBox.TabIndex = 3;
            serverAddressComboBox.SelectedIndexChanged += serverAddressComboBox_SelectedIndexChanged;
            // 
            // clientIdLabel
            // 
            clientIdLabel.AutoSize = true;
            clientIdLabel.Location = new Point(12, 106);
            clientIdLabel.Name = "clientIdLabel";
            clientIdLabel.RightToLeft = RightToLeft.No;
            clientIdLabel.Size = new Size(66, 20);
            clientIdLabel.TabIndex = 4;
            clientIdLabel.Text = "Client ID";
            // 
            // clientSecretLabel
            // 
            clientSecretLabel.AutoSize = true;
            clientSecretLabel.Location = new Point(12, 146);
            clientSecretLabel.Name = "clientSecretLabel";
            clientSecretLabel.RightToLeft = RightToLeft.No;
            clientSecretLabel.Size = new Size(92, 20);
            clientSecretLabel.TabIndex = 5;
            clientSecretLabel.Text = "Client Secret";
            // 
            // clientIdTextBox
            // 
            clientIdTextBox.Location = new Point(135, 103);
            clientIdTextBox.Name = "clientIdTextBox";
            clientIdTextBox.Size = new Size(151, 27);
            clientIdTextBox.TabIndex = 6;
            clientIdTextBox.TextChanged += clientIdTextBox_TextChanged;
            // 
            // clientSecretTextBox
            // 
            clientSecretTextBox.Location = new Point(135, 143);
            clientSecretTextBox.Name = "clientSecretTextBox";
            clientSecretTextBox.Size = new Size(275, 27);
            clientSecretTextBox.TabIndex = 7;
            clientSecretTextBox.UseSystemPasswordChar = true;
            clientSecretTextBox.TextChanged += clientSecretTextBox_TextChanged;
            // 
            // connectButton
            // 
            connectButton.Location = new Point(268, 251);
            connectButton.Name = "connectButton";
            connectButton.Size = new Size(94, 29);
            connectButton.TabIndex = 8;
            connectButton.Text = "Connect";
            connectButton.UseVisualStyleBackColor = true;
            connectButton.Click += connectButton_Click;
            // 
            // loadDefaultConnectSettingsButton
            // 
            loadDefaultConnectSettingsButton.Location = new Point(12, 198);
            loadDefaultConnectSettingsButton.Name = "loadDefaultConnectSettingsButton";
            loadDefaultConnectSettingsButton.Size = new Size(161, 29);
            loadDefaultConnectSettingsButton.TabIndex = 9;
            loadDefaultConnectSettingsButton.Text = "Load Default Settings";
            loadDefaultConnectSettingsButton.UseVisualStyleBackColor = true;
            loadDefaultConnectSettingsButton.Click += loadDefaultConnectSettingsButton_Click;
            // 
            // saveConnectSettingsButton
            // 
            saveConnectSettingsButton.Location = new Point(201, 198);
            saveConnectSettingsButton.Name = "saveConnectSettingsButton";
            saveConnectSettingsButton.Size = new Size(161, 29);
            saveConnectSettingsButton.TabIndex = 10;
            saveConnectSettingsButton.Text = "Save as Default";
            saveConnectSettingsButton.UseVisualStyleBackColor = true;
            saveConnectSettingsButton.Click += saveConnectSettingsButton_Click;
            // 
            // ConnectForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(422, 297);
            Controls.Add(saveConnectSettingsButton);
            Controls.Add(loadDefaultConnectSettingsButton);
            Controls.Add(connectButton);
            Controls.Add(clientSecretTextBox);
            Controls.Add(clientIdTextBox);
            Controls.Add(clientSecretLabel);
            Controls.Add(clientIdLabel);
            Controls.Add(serverAddressComboBox);
            Controls.Add(serverAddressLabel);
            Controls.Add(apiInterfaceComboBox);
            Controls.Add(apiInterfaceLabel);
            Name = "ConnectForm";
            Text = "Connection Settings";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label apiInterfaceLabel;
        private ComboBox apiInterfaceComboBox;
        private Label serverAddressLabel;
        private ComboBox serverAddressComboBox;
        private Label clientIdLabel;
        private Label clientSecretLabel;
        private TextBox clientIdTextBox;
        private TextBox clientSecretTextBox;
        private Button connectButton;
        private Button loadDefaultConnectSettingsButton;
        private Button saveConnectSettingsButton;
    }
}