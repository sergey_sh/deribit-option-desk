﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Deribit.Objects;


namespace deribit_option_desk
{
    public partial class ConnectForm : Form
    {
        public event EventHandler? StartConnection;

        public ConnectForm()
        {
            InitializeComponent();

            //load default connection settings
            loadDefaultConnectSettings();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            connectButton.Enabled = false;
            StartConnection?.Invoke(this, EventArgs.Empty);
            this.Close();
        }

        private void loadDefaultConnectSettingsButton_Click(object sender, EventArgs e)
        {
            loadDefaultConnectSettings();
        }

        private void saveConnectSettingsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(
                "Do you want to override existing default parameters?",
                "Confirmation",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information
                );
            if (res == DialogResult.OK)
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@"connection_settings.json"))
                {
                    ConnectSettings settings = ConnectSettings.CurrentConnectSettings;
                    
                    settings.apiInterface = apiInterfaceComboBox.Text;
                    settings.serverAddress = serverAddressComboBox.Text;
                    settings.clientID = clientIdTextBox.Text;
                    settings.clientSecret = clientSecretTextBox.Text;

                    string jsonString = JsonConvert.SerializeObject(settings, Formatting.Indented);

                    writer.Write(jsonString);
                }
            }
        }

        public void UpdateConnectSettings()
        {
            ConnectSettings.CurrentConnectSettings.apiInterface = apiInterfaceComboBox.Text;
            ConnectSettings.CurrentConnectSettings.serverAddress = serverAddressComboBox.Text;
            ConnectSettings.CurrentConnectSettings.clientID = clientIdTextBox.Text;
            ConnectSettings.CurrentConnectSettings.clientSecret = clientSecretTextBox.Text;
        }
        public void loadDefaultConnectSettings()
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"connection_settings.json"))
            {
                string res = reader.ReadToEnd();

                var settings = JsonConvert.DeserializeObject<ConnectSettings>(res);

                if (settings != null)
                {
                    apiInterfaceComboBox.SelectedItem = settings.apiInterface;
                    serverAddressComboBox.SelectedItem = settings.serverAddress;
                    clientIdTextBox.Text = settings.clientID;
                    clientSecretTextBox.Text = settings.clientSecret;
                    UpdateConnectSettings();
                }
            }
        }

        private void apiInterfaceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateConnectSettings();
        }

        private void serverAddressComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateConnectSettings();
        }

        private void clientIdTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateConnectSettings();
        }

        private void clientSecretTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateConnectSettings();
        }
    }
}
